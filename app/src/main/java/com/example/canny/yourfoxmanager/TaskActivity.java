package com.example.canny.yourfoxmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TaskActivity extends AppCompatActivity {

    Button buttonAddSub;
    EditText editTextSubName;
    SeekBar seekBarRating;
    TextView textViewRating, textViewTask;
    ListView listViewSubs;

    DatabaseReference databaseSubs;

    List<SubTask> subs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node sub we are creating a new child with the task id
         * and inside that node we will store all the subs with unique ids
         * */
        databaseSubs = FirebaseDatabase.getInstance().getReference("subs").child(intent.getStringExtra(MainActivity.TASK_ID));

        buttonAddSub = findViewById(R.id.buttonAddSub);
        editTextSubName = findViewById(R.id.editTextName);
        seekBarRating = findViewById(R.id.seekBarRating);
        textViewRating = findViewById(R.id.textViewRating);
        textViewTask = findViewById(R.id.textViewTask);
        listViewSubs = findViewById(R.id.listViewSubs);

        subs = new ArrayList<>();

        textViewTask.setText(intent.getStringExtra(MainActivity.TASK_NAME));

        seekBarRating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textViewRating.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        buttonAddSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSub();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseSubs.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                subs.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    SubTask sub = postSnapshot.getValue(SubTask.class);
                    subs.add(sub);
                }
                SubList subListAdapter = new SubList(TaskActivity.this, subs);
                listViewSubs.setAdapter(subListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveSub() {
        String subName = editTextSubName.getText().toString().trim();
        int rating = seekBarRating.getProgress();
        if (!TextUtils.isEmpty(subName)) {
            String id  = databaseSubs.push().getKey();
            SubTask sub = new SubTask(id, subName, rating);
            databaseSubs.child(id).setValue(sub);
            Toast.makeText(this, "SubTask saved", Toast.LENGTH_LONG).show();
            editTextSubName.setText("");
        } else {
            Toast.makeText(this, "Please enter sub name", Toast.LENGTH_LONG).show();
        }
    }
}

