package com.example.canny.yourfoxmanager;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class SubTask {
    private String id;
    private String subName;
    private int rating;

    public SubTask() {

    }

    public SubTask(String id, String subName, int rating) {
        this.subName = subName;
        this.rating = rating;
        this.id = id;
    }

    public String getSubName() {
        return subName;
    }

    public int getRating() {
        return rating;
    }
}
