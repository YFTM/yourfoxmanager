package com.example.canny.yourfoxmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TASK_NAME = "net.vitalii.sanin.taskname";
    public static final String TASK_ID = "net.vitalii.sanin.taskid";

    EditText editTextName;
    Spinner spinnerSub;
    Button buttonAddTask;
    ListView listViewTasks;
    
    List<Task> tasks;
    private FirebaseAuth mAuth;

    DatabaseReference databaseTasks;

    FirebaseUser user = mAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseTasks = FirebaseDatabase.getInstance().getReference(user.getUid()).child("tasks");

        editTextName = findViewById(R.id.editTextName);
        spinnerSub = findViewById(R.id.spinnerSubs);
        listViewTasks = findViewById(R.id.listViewTasks);
        buttonAddTask = findViewById(R.id.buttonAddTask);
        
        tasks = new ArrayList<>();


        //adding an onclicklistener to button
        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTask();
            }
        });

        //attaching listener to listview
        listViewTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected task
                Task task = tasks.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);

                //putting task name and id to intent
                intent.putExtra(TASK_ID, task.getTaskId());
                intent.putExtra(TASK_NAME, task.getTaskName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewTasks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Task task = tasks.get(i);
                showUpdateDeleteDialog(task.getTaskId(), task.getTaskName());
                return true;
            }
        });


    }

    private void showUpdateDeleteDialog(final String taskId, String taskName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = dialogView.findViewById(R.id.editTextName);
        final Spinner spinnerSub = dialogView.findViewById(R.id.spinnerSubs);
        final Button buttonUpdate = dialogView.findViewById(R.id.buttonUpdateTask);
        final Button buttonDelete = dialogView.findViewById(R.id.buttonDeleteTask);

        dialogBuilder.setTitle(taskName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String sub = spinnerSub.getSelectedItem().toString();
                if (!TextUtils.isEmpty(name)) {
                    updateTask(taskId, name, sub);
                    b.dismiss();
                }
            }
        });


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteTask(taskId);
                b.dismiss();
            }
        });
    }

    private boolean updateTask(String id, String name, String sub) {
        //getting the specified task reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(user.getUid()).child("tasks").child(id);

        //updating task
        Task task = new Task(id, name, sub);
        dR.setValue(task);
        Toast.makeText(getApplicationContext(), "Task Updated", Toast.LENGTH_LONG).show();
        return true;
    }

    private boolean deleteTask(String id) {
        //getting the specified task reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(user.getUid()).child("tasks").child(id);

        //removing task
        dR.removeValue();

        //getting the subs reference for the specified task
        DatabaseReference drSubs = FirebaseDatabase.getInstance().getReference("subs").child(id);

        //removing all subs
        drSubs.removeValue();
        Toast.makeText(getApplicationContext(), "Task Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseTasks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous task list
                tasks.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting task
                    Task task = postSnapshot.getValue(Task.class);
                    //adding task to the list
                    tasks.add(task);
                }

                //creating adapter
                TasksList taskAdapter = new TasksList(MainActivity.this, tasks);
                //attaching adapter to the listview
                listViewTasks.setAdapter(taskAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /*
     * This method is saving a new task to the
     * Firebase Realtime Database
     * */
    private void addTask() {
        //getting the values to save
        String name = editTextName.getText().toString().trim();
        String sub = spinnerSub.getSelectedItem().toString();

        //checking if the value is provided
        if (!TextUtils.isEmpty(name)) {

            //getting a unique id using push().getKey() method
            //it will create a unique id and we will use it as the Primary Key for our Task
            String id = databaseTasks.push().getKey();

            //creating an Task Object
            Task task = new Task(id, name, sub);

            //Saving the Task
            databaseTasks.child(id).setValue(task);

            //setting edittext to blank again
            editTextName.setText("");

            //displaying a success toast
            Toast.makeText(this, "Task added", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }
    }
}
