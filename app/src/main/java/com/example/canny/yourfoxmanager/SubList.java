package com.example.canny.yourfoxmanager;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class SubList extends ArrayAdapter<SubTask> {
    private Activity context;
    List<SubTask> subs;

    public SubList(Activity context, List<SubTask> subs) {
        super(context, R.layout.layout_task_list, subs);
        this.context = context;
        this.subs = subs;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_task_list, null, true);

        TextView textViewName = listViewItem.findViewById(R.id.textViewName);
        TextView textViewRating = listViewItem.findViewById(R.id.textViewSub);

        SubTask sub = subs.get(position);
        textViewName.setText(sub.getSubName());
        textViewRating.setText(String.valueOf(sub.getRating()));

        return listViewItem;
    }
}