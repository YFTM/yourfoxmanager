package com.example.canny.yourfoxmanager;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Task {
    private String taskId;
    private String taskName;
    private String taskSub;

    public Task(){
        //this constructor is required
    }

    public Task(String taskId, String taskName, String taskSub) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.taskSub = taskSub;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskSub() {
        return taskSub;
    }
}
